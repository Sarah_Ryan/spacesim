﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script deals will all the vector and matrix math present in the flight simulator. This also includes the definition of a 4 x 4 matrix and matrix multiplication
//It is used by the rocketMovement script and the Transformations script
public class VectorMath : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//Vector maths

	//Adds 2 vector3 variables together
    public static Vector3 addVectors(Vector3 firstVector, Vector3 secondVector)
	{
		//Adds the x components
		float xPos = firstVector.x + secondVector.x;
		
		//Adds the y components
		float yPos = firstVector.y + secondVector.y;
        
		//Adds the z components
        float zPos = firstVector.z + secondVector.z;

		//Creates a new vector3 that holds the resulting x, y and z positions from the addition
		Vector3 newVector = new Vector3(xPos, yPos, zPos);

        return newVector;
    }

	//Subtracts 2 vector3 variables from each other. 
	//Functions similarly to the addVectors function.
    public static Vector3 subtractVectors(Vector3 firstVector, Vector3 secondVector)
    {
        float xPos = firstVector.x - secondVector.x;
        float yPos = firstVector.y - secondVector.y;
        float zPos = firstVector.z - secondVector.z;

        Vector3 newVector = new Vector3(xPos, yPos, zPos);

        return newVector;
    }
    
	//Finds the length of a given vector3
    public static float vectorLength(Vector3 firstVector)
	{
		//X component multiplied by itself to get the squared value
		float a2 = (firstVector.x * firstVector.x);
		
		//Y component multiplied by itself to get the squared value
		float b2 = (firstVector.y * firstVector.y);

		//Z component multiplied by itself to get the squared value
		float c2 = (firstVector.z * firstVector.z);

		//The squared values are added together 
		float d2 = b2 + a2 + c2;

		//The length is square rooted to get the actual length
		float length = Mathf.Sqrt(d2);

        return length;

    }

	//Multiplies a vector3 by a single scalar value
    public static Vector3 multiplyVectorScalar(Vector3 firstVector, float scalar)
	{
		//Each component is scaled up by the scalar through multiplication
        float xPos = firstVector.x * scalar;
        float yPos = firstVector.y * scalar;
        float zPos = firstVector.z * scalar;

		//Results are stored in a new vector3 and returned
        Vector3 newVector = new Vector3(xPos, yPos, zPos);

        return newVector;

    }

	//Divides a vector3 by a single scalar value
	//This works similiarly to the multiplication function
    public static Vector3 divideVector(Vector3 firstVector, float divisor)
    {

        float xPos = firstVector.x / divisor;
        float yPos = firstVector.y / divisor;
        float zPos = firstVector.z / divisor;

        Vector3 newVector = new Vector3(xPos, yPos, zPos);

        return newVector;
    }
	
	//Vector normalisation. This creates a unit vector thatcan be used to calculate the dot product
    public static Vector3 normaliseVector(Vector3 firstVector)
	{
		//Retrieves the length of the vector
        float length = vectorLength(firstVector);

		//Divides the vector by its length and returns the value.
		Vector3 normalisedVector = divideVector(firstVector, length);
        
        return normalisedVector;
    }

	//Calculates the cross product 
    public static Vector3 CrossProduct(Vector3 firstVector, Vector3 secondVector)
    {
        Vector3 resultV;

        resultV.x = (firstVector.y * secondVector.z) - (firstVector.z * secondVector.y);
        resultV.y = (firstVector.z * secondVector.x) - (firstVector.x * secondVector.z);
        resultV.z = (firstVector.x * secondVector.y) - (firstVector.y * secondVector.x);

        return resultV;
    }

	//Calculates the euler angles of the game object
	//It takes a single parameter, this is a string of the name of the object within the Unity editor
    public static Vector3 calculateEuler(string objectName)
	{
		//It uses Unit's position to get the Euler angles of the object
        Vector3 charRot = GameObject.Find(objectName).transform.rotation.eulerAngles;

        return charRot;
    }

	//This creates a euler vector using the euler angles of the object
	//This is used to create the forward and right direction vectors
    public static Vector3 EulerVector(Vector3 eulerAngles)
    {
        Vector3 directionVector;

        directionVector.z = Mathf.Cos(eulerAngles.y) * Mathf.Cos(eulerAngles.x);
        directionVector.y = Mathf.Sin(eulerAngles.x);
        directionVector.x = Mathf.Cos(eulerAngles.x) * Mathf.Sin(eulerAngles.y);

        return directionVector;
    }
    
	//Conversion function to change degrees into radians
	public static float degreesToRadians(float angle){
		
		//The angle is divided by the result of 180 divide by PI
		angle = angle/(180/Mathf.PI);
		
		return angle;
	}
}

	//Creates a class for a 4 x 4 matrix
	public class Matrix4by4
	{
    	public Matrix4by4(Vector4 column1, Vector4 column2, Vector4 column3, Vector4 column4)
    	{
        values = new float[4, 4];

	    //Each vector 4 is applied to create the 4 columns of the matrix
	    
        //Column 1
        values[0, 0] = column1.x;
        values[1, 0] = column1.y;
        values[2, 0] = column1.z;
        values[3, 0] = column1.w;

        //Column 2
        values[0, 1] = column2.x;
        values[1, 1] = column2.y;
        values[2, 1] = column2.z;
        values[3, 1] = column2.w;

        //Column 3
        values[0, 2] = column3.x;
        values[1, 2] = column3.y;
        values[2, 2] = column3.z;
        values[3, 2] = column3.w;

        //Column 4
        values[0, 3] = column4.x;
        values[1, 3] = column4.y;
        values[2, 3] = column4.z;
        values[3, 3] = column4.w;
    }

    public float[,] values;

    public static Vector4 operator *(Matrix4by4 lhs, Vector4 vector)
    {
        Vector4 result = new Vector4();
        vector.w = 1;

	    //This multiplies a 4 x 4 matrix by a vector4, resulting in vector4
	    //Each component of the vector is multiplied by each value in a row.
	    //This results in the x, y, z or w of a vector4 depending on the row
	    
	    //X component
        result.x += lhs.values[0, 0] * vector.x;
        result.x += lhs.values[0, 1] * vector.y;
        result.x += lhs.values[0, 2] * vector.z;
        result.x += lhs.values[0, 3] * vector.w;

	    //Y component
        result.y += lhs.values[1, 0] * vector.x;
        result.y += lhs.values[1, 1] * vector.y;
        result.y += lhs.values[1, 2] * vector.z;
        result.y += lhs.values[1, 3] * vector.w;

	    //Z component
        result.z += lhs.values[2, 0] * vector.x;
        result.z += lhs.values[2, 1] * vector.y;
        result.z += lhs.values[2, 2] * vector.z;
        result.z += lhs.values[2, 3] * vector.w;

	    //W component
        result.w += lhs.values[3, 0] * vector.x;
        result.w += lhs.values[3, 1] * vector.y;
        result.w += lhs.values[3, 2] * vector.z;
        result.w += lhs.values[3, 3] * vector.w;

        return result;
    }

		//This function multiplies two matrices together. This overloads the multiplication operator
		//The row of the first matrix is multiplied by each column of the second matrix
    public static Matrix4by4 operator *(Matrix4by4 first, Matrix4by4 second)
		{

        //These variables will be the rows of the resulting matrix
        Vector4 firstRow = new Vector4();
        Vector4 secondRow = new Vector4();
        Vector4 thirdRow = new Vector4();
        Vector4 fourthRow = new Vector4();

        //First row calculations
        firstRow.x += first.values[0, 0] * second.values[0, 0];
        firstRow.x += first.values[0, 1] * second.values[1, 0];
        firstRow.x += first.values[0, 2] * second.values[2, 0];
        firstRow.x += first.values[0, 3] * second.values[3, 0];

        firstRow.y += first.values[0, 0] * second.values[0, 1];
        firstRow.y += first.values[0, 1] * second.values[1, 1];
        firstRow.y += first.values[0, 2] * second.values[2, 1];
        firstRow.y += first.values[0, 3] * second.values[3, 1];

        firstRow.z += first.values[0, 0] * second.values[0, 2];
        firstRow.z += first.values[0, 1] * second.values[1, 2];
        firstRow.z += first.values[0, 2] * second.values[2, 2];
        firstRow.z += first.values[0, 3] * second.values[3, 2];

        firstRow.w += first.values[0, 0] * second.values[0, 3];
        firstRow.w += first.values[0, 1] * second.values[1, 3];
        firstRow.w += first.values[0, 2] * second.values[2, 3];
        firstRow.w += first.values[0, 3] * second.values[3, 3];

        //Second row calculations
        secondRow.x += first.values[1, 0] * second.values[0, 0];
        secondRow.x += first.values[1, 1] * second.values[1, 0];
        secondRow.x += first.values[1, 2] * second.values[2, 0];
        secondRow.x += first.values[1, 3] * second.values[3, 0];

        secondRow.y += first.values[1, 0] * second.values[0, 1];
        secondRow.y += first.values[1, 1] * second.values[1, 1];
        secondRow.y += first.values[1, 2] * second.values[2, 1];
        secondRow.y += first.values[1, 3] * second.values[3, 1];

        secondRow.z += first.values[1, 0] * second.values[0, 2];
        secondRow.z += first.values[1, 1] * second.values[1, 2];
        secondRow.z += first.values[1, 2] * second.values[2, 2];
        secondRow.z += first.values[1, 3] * second.values[3, 2];

        secondRow.w += first.values[1, 0] * second.values[0, 3];
        secondRow.w += first.values[1, 1] * second.values[1, 3];
        secondRow.w += first.values[1, 2] * second.values[2, 3];
        secondRow.w += first.values[1, 3] * second.values[3, 3];

        //Third row calculations
        thirdRow.x += first.values[2, 0] * second.values[0, 0];
        thirdRow.x += first.values[2, 1] * second.values[1, 0];
        thirdRow.x += first.values[2, 2] * second.values[2, 0];
        thirdRow.x += first.values[2, 3] * second.values[3, 0];

        thirdRow.y += first.values[2, 0] * second.values[0, 1];
        thirdRow.y += first.values[2, 1] * second.values[1, 1];
        thirdRow.y += first.values[2, 2] * second.values[2, 1];
        thirdRow.y += first.values[2, 3] * second.values[3, 1];

        thirdRow.z += first.values[2, 0] * second.values[0, 2];
        thirdRow.z += first.values[2, 1] * second.values[1, 2];
        thirdRow.z += first.values[2, 2] * second.values[2, 2];
        thirdRow.z += first.values[2, 3] * second.values[3, 2];

        thirdRow.w += first.values[2, 0] * second.values[0, 3];
        thirdRow.w += first.values[2, 1] * second.values[1, 3];
        thirdRow.w += first.values[2, 2] * second.values[2, 3];
        thirdRow.w += first.values[2, 3] * second.values[3, 3];

        //Fourth row calculations
        fourthRow.x += first.values[3, 0] * second.values[0, 0];
        fourthRow.x += first.values[3, 1] * second.values[1, 0];
        fourthRow.x += first.values[3, 2] * second.values[2, 0];
        fourthRow.x += first.values[3, 3] * second.values[3, 0];

        fourthRow.y += first.values[3, 0] * second.values[0, 1];
        fourthRow.y += first.values[3, 1] * second.values[1, 1];
        fourthRow.y += first.values[3, 2] * second.values[2, 1];
        fourthRow.y += first.values[3, 3] * second.values[3, 1];

        fourthRow.z += first.values[3, 0] * second.values[0, 2];
        fourthRow.z += first.values[3, 1] * second.values[1, 2];
        fourthRow.z += first.values[3, 2] * second.values[2, 2];
        fourthRow.z += first.values[3, 3] * second.values[3, 2];

        fourthRow.w += first.values[3, 0] * second.values[0, 3];
        fourthRow.w += first.values[3, 1] * second.values[1, 3];
        fourthRow.w += first.values[3, 2] * second.values[2, 3];
        fourthRow.w += first.values[3, 3] * second.values[3, 3];




        Matrix4by4 result = new Matrix4by4(firstRow, secondRow, thirdRow, fourthRow);

        return result;
    }

   
}
