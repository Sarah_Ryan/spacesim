﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script handles all the movement for the spaceship, from user input.

public class rocketMovement : MonoBehaviour
{

    //Variable declarations

    //Global up vector, used in calculation of the rightDirection vector
    Vector3 globalUpVec = new Vector3(0, 1, 0);

    //Vector that will store the euler angle of the rigidbody
    Vector3 eulerRotation = new Vector3();
    //Vector that will store the forward direction vector of the spaceship
    Vector3 forwardDirection = new Vector3();
    //Vector that will store the right direction vector of the spaceship
    Vector3 rightDirection = new Vector3();
    //Reference to the spaceship object name
	string spaceshipName = "Ship";

    public GameObject camera;

    Vector3 offset;
    Vector3 offsetY;

    //Reference to the transformation script
    //This script contains all the matrix calculations to transform the spaceship
    public Transformations transformScript;

    //Linear velocity
    Vector3 velocity;

    //Position value of spaceship
	Vector3 position;
    
	//Ship mass
	int shipMass = 1000;

    //Angle that the ship should be rotated by
    float yawAngle;
    float pitchAngle;
	float rollAngle;

    //Constants for the thruster and engine power
    //The engine power is larger than the thrusters and will apply a greater force
    float mainThrust = 0.05f;
    float mainEngine = 0.1f;

	//Angular velocity values
	float yawVelocity;
	float pitchVelocity;
	float rollVelocity;

    // Use this for initialization
    void Start()
    {

        offset = new Vector3(0, 2, -3);
        offsetY = new Vector3(0, 1, 0);

        camera.transform.position = transform.position + offset;
       
	    //Convert the euler angle from degrees into radians
	    eulerRotation.x *= VectorMath.degreesToRadians(eulerRotation.x);
	    eulerRotation.y *= VectorMath.degreesToRadians(eulerRotation.y);
	    eulerRotation.z *= VectorMath.degreesToRadians(eulerRotation.z);
	    
        //Changes the force of the engine and thrusters by dividing them by the ship's mass
        mainThrust /= shipMass;
        mainEngine /= shipMass;
    }

    // Update is called once per frame
    void FixedUpdate()
	{
        
		//Assigns the spaceships euler angles into a new vector3
        eulerRotation = VectorMath.calculateEuler(spaceshipName);

		//Calculates the spaceship forward direction
        forwardDirection = VectorMath.EulerVector(eulerRotation);

		//Calculates the spaceship's right direction by getting the cross product of a global up
		//and the forward direction
        rightDirection = VectorMath.CrossProduct(globalUpVec, forwardDirection);

		//Main engine movement. Linear Motion
		//The method of applying a force value to a direction seen below is the same for all movement that has linear velocity
		//It will be explained the first time but not after.
		
		//Main engine forward movement (Z Axis)
        if (Input.GetKey(KeyCode.W))
        {
        	
            //The velocity of the spaceship is changed by adding a force in forward direction
	        velocity += forwardDirection * mainEngine;
            

        }

		//Main engine backward movement (-Z Axis)
        if (Input.GetKey(KeyCode.S))
        {
        	//The velocity of the spaceship is changed by subtracting a force in the forward direction
        	//Results in velocity being applied in the opposite of the forward direction
            velocity -= forwardDirection * mainEngine;
         
        }
   
		
		//Strafing Movement
		
		//Left strafing using 2 thrusters
		if (Input.GetKey(KeyCode.E))
        {
            velocity -= rightDirection * mainThrust;

        }

		//Right strafing using 2 thrusters
		if (Input.GetKey(KeyCode.Q))
        {
            velocity += rightDirection * mainThrust;

        }
        
		//Vertical Movement

		//Positive Vertical Movement
        if (Input.GetKey(KeyCode.LeftShift))
        {
            velocity += transform.up * mainThrust;
        }

		//Negative Vertical Movement
        if (Input.GetKey(KeyCode.LeftControl))
        {
            velocity -= transform.up * mainThrust;
        }
        
		//Rotations
		//Rotation is calculated by applying an acceleration value to a rotational velocity

		//Positive yaw rotation
		if (Input.GetKey(KeyCode.A))
        {
	        //Acceleration applied to yaw rotational velocity
	        yawVelocity -= mainThrust;

        }

		//Negative yaw rotation
		if (Input.GetKey(KeyCode.D))
        {

	        yawVelocity += mainThrust;

        }

		//Positive pitch rotation
		if (Input.GetKey(KeyCode.UpArrow))
        {
	        pitchVelocity += mainThrust;
        }

		//Negative pitch rotation
		if (Input.GetKey(KeyCode.DownArrow))
        {
	        pitchVelocity -= mainThrust;
        }

		//Positive roll rotation
        if (Input.GetKey(KeyCode.LeftArrow))
        {
	        rollVelocity += mainThrust;
        }

		//Negative roll rotation
        if (Input.GetKey(KeyCode.RightArrow))
        {
	        rollVelocity -= mainThrust;
        }

		//Applies velocity to a position vector3. 
		//The values in the position vector will be put in to the objects translation matrix
		position += velocity;
        
		//The angle veclotities are applied to their corresponding angle float.
		//These angle floats will be added into their corresponding rotation matrices
        yawAngle += yawVelocity;
        pitchAngle += pitchVelocity;
        rollAngle += rollVelocity;

		//This calls a function in the Transformations script object 
		//It takes in 3 angle floats, these are for pitch, yaw and roll and are used to create the rotation matrix (R)
		//It also takes the x,y and z values from the position vector, these will be used to create the translate matrix (T)
	    transformScript.Transform(pitchAngle, yawAngle, rollAngle, position.x, position.y, position.z);


    }
}
