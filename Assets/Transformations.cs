﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This script deals with all rotations and translations of the spaceship object
//The Matrix4By4 is a class I have created myself and can be found in the vectorMath script
//The matrix multiplication also uses a method I created that overloads the multiplication operator, this can also be found in the vectorMath script
public class Transformations : MonoBehaviour {
	
	//Variable initialisiation
	
	//Vector3 array that will hold the vertices of the spaceship model
	Vector3[] ModelSpaceVertices;
	
    
	// Use this for initialization
	void Start () {

        //Mesh filter stores info about current mesh attached to the object in Unity
        MeshFilter MF = GetComponent<MeshFilter>();

        //Get a copy of all vertices of mesh and store it in a Vector3 array
        ModelSpaceVertices = MF.mesh.vertices;

    }
	
	// Update is called once per frame
	void Update () {

      

	}
    
	//The Transform function creates an identity scale matrix, a translate matrix and a rotation matrix.
	//The function then combines all the matrices into a single transform matrix
    public void Transform(float pitchAngle, float yawAngle, float rollAngle, float xPos, float yPos, float zPos)
    {
        //New array with the correct size for vertices of mesh
	    Vector3[] TransformedVertices = new Vector3[ModelSpaceVertices.Length];
        
	    //Define a scale identity matrix
	    //The scale matrix is an identity matrix because it will never change, the spaceship cannot change in size.
	    Matrix4by4 S = new Matrix4by4(
	    	new Vector4(1, 0, 0, 0),
	    	new Vector4(0, 1, 0, 0),
	    	new Vector4(0, 0, 1, 0),
	    	new Vector4(0, 0, 0, 1));
        
	    //Defined translation matrix
	    //The translate matrix is similar to an identity matrix however it includes the x, y and z position of the object.
	    //This will make the spaceship move
	    Matrix4by4 T = new Matrix4by4(
		    new Vector4(1, 0, 0, xPos),
	    	new Vector4(0, 1, 0, yPos),
	    	new Vector4(0, 0, 1, zPos),
	    	new Vector4(0, 0, 0, 1));


	    //Pitch matrix definition
	    //This usess the pitchAngle float to define matrix values
	    Matrix4by4 pitchMatrix = new Matrix4by4(
		    new Vector4(1, 0, 0, 0),
		    new Vector4(0, Mathf.Cos(pitchAngle), Mathf.Sin(pitchAngle), 0),
		    new Vector4(0, -Mathf.Sin(pitchAngle), Mathf.Cos(pitchAngle), 0),
		    new Vector4(0, 0, 0, 0));
		    
		 //Yaw matrix definition
	     //This usess the yawAngle float to define matrix values
	    Matrix4by4 yawMatrix = new Matrix4by4(
		    new Vector4(Mathf.Cos(yawAngle), 0, -Mathf.Sin(yawAngle), 0),
		    new Vector4(0, 1, 0, 0),
		    new Vector4(Mathf.Sin(yawAngle), 0, Mathf.Cos(yawAngle), 0),
		    new Vector4(0, 0, 0, 1));
		    
	    //Roll matrix definition
	    //This usess the rollAngle float to define matrix values
        Matrix4by4 rollMatrix = new Matrix4by4(
            new Vector4(Mathf.Cos(rollAngle), Mathf.Sin(rollAngle), 0, 0),
            new Vector4(-Mathf.Sin(rollAngle), Mathf.Cos(rollAngle), 0, 0),
            new Vector4(0, 0, 1, 0),
            new Vector4(0, 0, 0, 0));

	    //The rotation matrices are combined into a single rotation matrix
        Matrix4by4 R = yawMatrix * (pitchMatrix * rollMatrix);

	    //The translation matrix (T) is multiplied by the result of R * S
	    //This creates a single matrix (M), which contains scaling, rotation and translation information
	    Matrix4by4 M = T * (R * S);

	    //Transforms each individual model space vertices by the M matrix
        for (int i = 0; i < TransformedVertices.Length; i++)
        {
            TransformedVertices[i] = M * ModelSpaceVertices[i];
        }

        MeshFilter MF = GetComponent<MeshFilter>();

        //Assign new vertices to mesh
        MF.mesh.vertices = TransformedVertices;

        //Makes mesh look correct
        MF.mesh.RecalculateNormals();
        MF.mesh.RecalculateBounds();
    }

}
